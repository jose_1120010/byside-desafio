
package bysidedesafio;

import business.FileSystem;
import business.Folder;
import business.Item;
import ui.CommandLineMenu;

/**
 *
 * @author jose
 */
public class BySideDesafio {

    public static void main(String[] args) {
        
        FileSystem fs = new FileSystem("ext4", "jose");
        fs.populateFS();
        
        CommandLineMenu menu = new CommandLineMenu();
        menu.showMainMenu(fs);
    }
    
}
