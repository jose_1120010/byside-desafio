package ui;

import business.File;
import business.FileSystem;
import business.Folder;
import business.Item;
import java.util.Date;
import java.util.Scanner;

/**
 *
 * @author jose
 */
public class CommandLineMenu implements Menu {

    private static int nOption;
    private int selectedFile;

    public CommandLineMenu() {
        this.selectedFile = 0; // no file selected
        CommandLineMenu.nOption = 0;
    }

    @Override
    public void showMainMenu(FileSystem fs) {
        String answer;
        do {
            System.out.println("User '" + fs.getCurrentUser() + "' is logged in...\n");
            displayAllItems(fs);
            if (fileIsSelected()) {
                displayOptions(fs.getCachedFileById(selectedFile));
                
            } else {
                displayOptions(null);
            }
            answer = readMainMenuOption(fs.getFileCacheSize());
            switch(answer) 
            { 
                case "X": 
                    System.out.println("Bye, see you soon! =)");
                    break;
                case "A": 
                    createFile(fs);
                    break; 
                case "B": 
                    createFolder(fs);
                    break; 
                case "C": 
                    moveItem(fs);
                    break; 
                case "D": 
                    copyItem(fs);
                    break; 
                case "E": 
                    deleteItem(fs);
                    break; 
                case "F": 
                    displayFileDetails(fs);
                    break; 
                default:
                    if (answer.matches("[1-9][0-9]*")){
                        // select chosen file
                        this.selectedFile = Integer.parseInt(answer);
                        System.out.println("'" + fs.getCachedFileById(selectedFile).getName() + "' was selected");
                    }
            }
            if (!answer.equals("X")) {
                standByForInput();
            }
        } while (!answer.equals("X"));
    }
    
    private void createFile(FileSystem fs) {
        Folder destFolder = readDestinationFolder(fs);
        
        Scanner sc = new Scanner(System.in);
        String name, owner, fileType;
        System.out.println("New file name:");        
        name = sc.next();
        while (!name.matches("[a-z]+")) {
            System.out.println("Invalid file name.\nPlease choose a valid name using only lowercase letters:");        
            name = sc.next();
        }
        System.out.println("File extension:");  
        fileType = sc.next();
        while (!fileType.matches("[a-z]+[1-9]?")) {
            System.out.println("Invalid file extension.\nPlease choose a valid extension using only lowercase letters that may be followed numbers:");        
            fileType = sc.next();
        }
        System.out.println("Owner:");        
        owner = sc.next();
        while (!owner.matches("[a-z]+")) {
            System.out.println("Invalid owner.\nPlease choose a valid owner using only lowercase letters:");        
            owner = sc.next();
        }
        File newFile = fs.createFile(name, owner, fileType, destFolder);
        System.out.println("'" + newFile.getName() + "' created in '" + newFile.getParent().getName()+"'");
    }
    
    private void createFolder(FileSystem fs) {
        Folder parent = readDestinationFolder(fs);
        
        Scanner sc = new Scanner(System.in);
        String name, owner;
        System.out.println("New folder name:");        
        name = sc.next();
        while (!name.matches("[a-z]+")) {
            System.out.println("Invalid name.\nPlease choose a valid name using only lowercase letters:");        
            name = sc.next();
        }
        System.out.println("Owner:");        
        owner = sc.next();
        while (!owner.matches("[a-z]+")) {
            System.out.println("Invalid owner.\nPlease choose a valid owner using only lowercase letters:");        
            owner = sc.next();
        }
        Folder newFolder = fs.createFolder(name, owner, parent);
        System.out.println("'" + newFolder.getName() + "' created in '" + newFolder.getParent().getName()+"'");
    }
    
    private Folder readDestinationFolder(FileSystem fs) {
        
        Scanner sc = new Scanner(System.in);
        String answer;
        System.out.println("Please choose the destination folder using a number OR 0 for root directory:");        
        answer = sc.next();
        while (!answer.matches("[0-9]+") ||
                Integer.parseInt(answer) > fs.getFileCacheSize() ||
                !answer.equals("0") && !(fs.getCachedFileById(Integer.parseInt(answer)) instanceof Folder)) {
            System.out.println("Invalid option.\nPlease choose the destination folder using a number OR 0 for root directory:");        
            answer = sc.next();
        }
        int folderId = Integer.parseInt(answer);
        Folder newParent;
        if (answer.equals("0")) {
            newParent = fs.getRootFolder();
        } else {
            newParent = (Folder)fs.getCachedFileById(folderId);
        }
        return newParent;
    }
    
    private void copyItem(FileSystem fs) {
        Item item = fs.getCachedFileById(selectedFile);
        Folder parent = readDestinationFolder(fs);
        if (fs.copyFile(item, parent)) {
            System.out.println("'"+item.getName()+"' copied to '"+parent.getName()+"'");
            resetSelectedFile();
        } else {
            System.out.println("You lack permissions to perform this operation. Owner is '"+item.getOwner()+"'");
        }
    }
    
    private void moveItem(FileSystem fs) {
        Item item = fs.getCachedFileById(selectedFile);
        Folder newParent = readDestinationFolder(fs);
        if (fs.moveFile(item, newParent)) {
            System.out.println("'"+item.getName()+"' moved to '"+newParent.getName()+"'");
            resetSelectedFile();
        } else {
            System.out.println("You lack permissions to perform this operation. Owner is '"+item.getOwner()+"'");
        }
    }

    private void deleteItem(FileSystem fs) {
        Item item = fs.getCachedFileById(selectedFile);
        if (fs.deleteFile(item)) {
            System.out.println("'"+item.getName()+"' deleted sucessfully");
            resetSelectedFile();
        } else {
            System.out.println("You lack permissions to perform this operation. Owner is '"+item.getOwner()+"'");
        }
    }
    
    @Override
    public void displayAllItems(FileSystem fs) {
        nOption = 0;
        System.out.println("option ------------FILES------------\n");
        fs.resetFileCache();
        // display file tree recursively
        if (fs.getRootFolder().hasChildren()) {
            displayChildren(fs.getRootFolder(), fs, "|__ ", -1);
        } else {
            System.out.println("There are no files to display");
        }
        System.out.println("");
    }

    private void displayChildren(Folder folder, FileSystem fs, String separator, int level) {
        level++;
        for (Item item : folder.getChildren()) {

            if (item.hasParent()) {
                nOption++;
                fs.addToFileCache(item);
                System.out.println(nOption + drawTreeBranches(separator, level) + item.getName() 
                        + (this.selectedFile == nOption ? " (selected)" : ""));

                if (item instanceof Folder && ((Folder) item).hasChildren()) {
                    displayChildren((Folder) item, fs, separator, level);
                }
            }
        }
    }

    @Override
    public void displayOptions(Item item) {
        System.out.println("A - Create new file");
        System.out.println("B - Create new folder");
        if (item != null) {
            System.out.println("C - Move '" + item.getName() + "' to...");
            System.out.println("D - Copy '" + item.getName() + "' to...");
            System.out.println("E - Delete '" + item.getName() + "'");
            System.out.println("F - View '" + item.getName() + "' details");
        }
        System.out.println("X - Exit file manager");
        System.out.println("");
    }

    @Override
    public String readMainMenuOption(int fileIndexSize) {
        System.out.println("Use a number to select a file or use a letter to operate on it.");
        System.out.println("Please choose a menu option: ");
        Scanner sc = new Scanner(System.in);
        String answer;
        String regexOperationOpt = "[A-" + (fileIsSelected() ? "F" : "B") +"]|X";
        // input validation
        answer = sc.next();
        while ((fileIndexSize == 0 ||
                !answer.matches("[1-9][0-9]*") ||
                Integer.parseInt(answer) > fileIndexSize) 
                && !answer.matches(regexOperationOpt)) {
            System.out.println("Invalid option, use number to select a file or a letter to perform an operation");
            answer = sc.next();
        }
        return answer;
    }
    
    private void displayFileDetails(FileSystem fs) {
        Item file = fs.getCachedFileById(selectedFile);
        System.out.println("");
        System.out.println("---"+(file instanceof File ? "FILE" : "FOLDER")+" INFO---");
        System.out.println("Name:     '" + file.getName() + "'");
        System.out.println("Owner:    '" + file.getOwner() + "'");
        System.out.println("Created:  '" + file.getDateCreated() + "'");
        System.out.println("Modified: '" + file.getDateModified() + "'");

        if (file instanceof File) {
            System.out.println("Accessed: '" + ((File) file).getDateAccessed()+ "'");
            ((File) file).setDateAccessed(new Date());
        }

        System.out.println("");
    }
    
    private boolean fileIsSelected() {
        return this.selectedFile != 0;
    }
    
    private void resetSelectedFile() {
        this.selectedFile = 0;
    }
   
    // HELPER UI METHODS
    
    private String drawTreeBranches(String separator, int level) {
        return " ".repeat(7 - String.valueOf(nOption).length()) + separator.repeat(level);
    }
    
    private void standByForInput() {
        System.out.println("Press ENTER to continue");
        Scanner sc = new Scanner(System.in);
        sc.nextLine();
    }
    
}
