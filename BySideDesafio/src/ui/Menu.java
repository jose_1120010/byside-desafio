package ui;

import business.FileSystem;
import business.Item;

/**
 *
 * @author jose
 */
public interface Menu {

    public void showMainMenu(FileSystem fs);

    public void displayAllItems(FileSystem fs);

    public void displayOptions(Item item);

    public String readMainMenuOption(int fileIndexSize);

}
