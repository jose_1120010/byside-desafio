package business;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * This class represents a File or a Folder
 *
 * @author jose
 *
 */
public abstract class Item {

    private String name;
    private final Date dateCreated;
    private Date dateModified;
    private String owner;
    private Folder parent;

    public Item(String name, String owner, Folder parent) {
        this.name = name;
        this.dateCreated = new Date();
        this.dateModified = new Date();
        this.owner = owner;
        if (parent != null) {
            setParent(parent);
        }
    }
    public abstract void copyTo(Folder parent);

    public void delete() {
        this.getParent().removeChild(this);
        this.parent = null;
    }

    public void moveTo(Folder newParent) {
        this.getParent().removeChild(this);
        this.setParent(newParent);
    }

    public boolean hasParent() {
        return this.parent != null;
    }

    // GETTERS & SETTERS
    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Folder getParent() {
        return this.parent;
    }

    public void setParent(Folder parent) {
        this.parent = parent;
        this.parent.addChild(this);
    }

    public String getOwner() {
        return owner;
    }

    public Date getDateCreated() {
        return dateCreated;
    }

    public Date getDateModified() {
        return dateModified;
    }

}
