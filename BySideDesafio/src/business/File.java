package business;

import java.util.Date;

/**
 *
 * @author jose
 */
public class File extends Item {

    private String fileType;
    private Date dateAccessed;

    public File(String name, String owner, String fileType, Folder parent) {
        super(name, owner, parent);
        this.fileType = fileType;
        this.dateAccessed = new Date();
    }

    @Override
    public void copyTo(Folder parent) {
        Item newFile = new File(super.getName() + "_copy", this.getOwner(), this.getFileType(), parent);
    }
    
    // GETTERS & SETTERS
    
    @Override
    public String getName() {
        return super.getName() + "." + this.fileType;
    }
    
    public void setDateAccessed(Date dateAccessed) {
        this.dateAccessed = dateAccessed;
    }

    public Date getDateAccessed() {
        return dateAccessed;
    }

    public String getFileType() {
        return fileType;
    }
}
