package business;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jose
 */
public class Folder extends Item {

    private List<Item> children;

    public Folder(String name, String owner, Folder parent) {
        super(name, owner, parent);
        this.children = new ArrayList<Item>();
    }
    
    @Override
    public void copyTo(Folder parent) {
        Item newFile = new Folder(this.getName() + "_copy", this.getOwner(), parent);
    }
    
    public boolean hasChildren() {
        return !this.children.isEmpty();
    }

    public void addChild(Item item) {
        this.children.add(item);
    }

    public void removeChild(Item item) {
        this.children.remove(item);
    }

    // GETTERS & SETTERS
    public List<Item> getChildren() {
        return children;
    }

    

}
