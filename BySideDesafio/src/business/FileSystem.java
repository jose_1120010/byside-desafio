package business;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author jose
 */
public class FileSystem {

    private String description;
    private String currentUser;
    private Folder rootFolder;
    private List<Item> fileCache;

    public FileSystem(String description, String currentUser) {
        this.description = description;
        this.currentUser = currentUser;
        this.rootFolder = new Folder("root-folder", "root", null);
        this.fileCache = new ArrayList<Item>();
    }

    public void populateFS() {
        Folder folder1 = new Folder("mammals", "maria", this.rootFolder);
        Folder folder2 = new Folder("birds", "jose", this.rootFolder);
        Folder folder3 = new Folder("monkeys", "jose", folder1);

        File file1 = new File("cat", "jose", "mp3", folder1);
        File file2 = new File("dog", "jose", "pdf", folder1);
        File file3 = new File("chicken", "jose", "wav", folder2);
        File file4 = new File("cow", "jose", "pdf", folder1);
        File file5 = new File("goat", "jose", "zip", folder1);
        File file6 = new File("duck", "jose", "mp3", folder2);
        File file7 = new File("turkey", "jose", "pdf", folder2);
        File file8 = new File("spider", "jose", "mp4", this.rootFolder);
        File file9 = new File("chimpanzee", "jose", "mp3", folder3);
        File file10 = new File("gorilla", "jose", "gif", folder3);
    }
    
    public File createFile(String name, String owner, String fileType, Folder parent) {
        File newFile = new File(name, owner, fileType, parent);
        return newFile;
    }
    
    public Folder createFolder(String name, String owner, Folder parent) {
        Folder newFolder = new Folder(name, owner, parent);
        return newFolder;
    }

    public boolean deleteFile(Item item) {
        if (item.getOwner().equals(currentUser)) {
            item.delete();
            return true;
        } else {
            return false;
        }
    }

    public boolean copyFile(Item item, Folder targetFolder) {
        if (item.getOwner().equals(currentUser)) {
            item.copyTo(targetFolder);
            return true;
        } else {
            return false;
        }
    }

    public boolean moveFile(Item item, Folder targetFolder) {
        if (item.getOwner().equals(currentUser)) {
            item.moveTo(targetFolder);
            return true;
        } else {
            return false;
        }
    }

    // FILE CACHE METHODS
    public void addToFileCache(Item item) {
        this.fileCache.add(item);
    }

    public void removeFromFileCache(Item item) {
        this.fileCache.add(item);
    }

    public void resetFileCache() {
        this.fileCache.clear();
    }

    public int getFileCacheSize() {
        return this.fileCache.size();
    }

    // GETTERS & SETTERS
    public String getDescription() {
        return description;
    }

    public String getCurrentUser() {
        return currentUser;
    }

    public Folder getRootFolder() {
        return rootFolder;
    }
    
    public Item getCachedFileById(int id) {
        return this.getFileCache().get(id-1);
    }

    public List<Item> getFileCache() {
        return fileCache;
    }

}
